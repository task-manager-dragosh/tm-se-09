package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class RemoveTaskCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "remove task";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(удаляет из проекта задачу)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String taskName = readWord("Введите название задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        @Nullable Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        @Nullable Task task = serviceLocator.getTaskService().find(serviceLocator.getCurrentUser().getId(), project.getId(), taskName);
        if (task == null) {
            log(MessageType.TASK_NOT_FOUND);
            return;
        }
        serviceLocator.getTaskService().remove(serviceLocator.getCurrentUser().getId(), task.getId());
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}