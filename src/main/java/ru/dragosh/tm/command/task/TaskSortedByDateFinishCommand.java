package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public final class TaskSortedByDateFinishCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "sort tasks by date finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(вывод задач отсортрованных по дате окончания)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId()) == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }

        serviceLocator.getTaskService().getSortedByDateFinish(serviceLocator.getCurrentUser().getId()).forEach(ConsoleUtil::taskOutput);
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}
