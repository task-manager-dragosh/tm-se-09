package ru.dragosh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.HashSet;
import java.util.Set;

public final class LogoutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(выход из аккаунта)";
    }

    @Override
    public void execute() {
        serviceLocator.setCurrentUser(new User("", "", RoleType.INCOGNITO));
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}
