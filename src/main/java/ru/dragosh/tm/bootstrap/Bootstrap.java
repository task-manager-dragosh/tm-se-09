package ru.dragosh.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.repository.UserRepositoryImplement;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.TaskServiceImplement;
import ru.dragosh.tm.service.UserServiceImplement;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    @Getter
    private final ProjectService projectService = new ProjectServiceImplement(new ProjectRepositoryImplement());
    @NotNull
    @Getter
    private final TaskService taskService = new TaskServiceImplement(new TaskRepositoryImplement());
    @NotNull
    @Getter
    private final UserService userService = new UserServiceImplement(new UserRepositoryImplement());
    @NotNull
    @Getter
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    @Getter
    @Setter
    private User currentUser = new User("","", RoleType.INCOGNITO);
    @NotNull
    private final List<Class<? extends AbstractCommand>> classes =
            new ArrayList<>(new Reflections("ru.dragosh.tm").getSubTypesOf(AbstractCommand.class));

    public void init() {
        prepareCommands();
        prepareUsers();
        start();
    }

    private void start() {
        ConsoleUtil.log(MessageType.WELCOME_MESSAGE);
        ConsoleUtil.log(MessageType.HELP_MESSAGE);
        while (true) {
            @NotNull String stringCommand = ConsoleUtil.readCommand().toLowerCase();
            @Nullable AbstractCommand command = commands.get(stringCommand);

            if (command == null) {
                ConsoleUtil.log(MessageType.WRONG_COMMAND);
                continue;
            }
            if (!hasAccess(command)) {
                ConsoleUtil.log(MessageType.PERMISSION_DENIED);
                continue;
            }
            try {
                command.execute();
            } catch (CommandExecutionException e) {
                handleException(e);
            }
        }
    }

    private void prepareCommands() {
        @NotNull final Predicate<Class<? extends AbstractCommand>> commandClassPredicate = (commandClass) -> {
            try {
                return (Class.forName(commandClass.getCanonicalName()).newInstance()) instanceof AbstractCommand;
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                System.out.println("MESSAGE -> Ошибка создания инстанса команды!");
            }
            return false;
        };
        classes.stream()
                .filter(commandClassPredicate)
                .forEach(commandClass -> {
                    try {
                        AbstractCommand abstractCommand = commandClass.newInstance();
                        abstractCommand.setServiceLocator(this);
                        commands.put(abstractCommand.getName(), abstractCommand);
                    } catch (InstantiationException | IllegalAccessException e) {
                        System.out.println("MESSAGE -> Ошибка создания инстанса команды!");
                    }
                });
    }

    private boolean hasAccess(@NotNull final AbstractCommand command) {
        return command.getRoles().contains(currentUser.getRole());
    }

    private void prepareUsers() {
        userService.persist(new User("root", "root", RoleType.ADMIN));
        userService.persist(new User("user", "user", RoleType.USER));
    }

    private void handleException(@NotNull final Exception ex) {
        if (ex instanceof  IllegalAccessException
                || ex instanceof InstantiationException
                || ex instanceof ClassNotFoundException) {
            System.out.println("Message -> Ошибка создания инстанса команды!");
            return;
        }
        if (ex instanceof CommandExecutionException)
            System.out.println("Message -> Ошибка во время выполнения команды!");
    }
}
