package ru.dragosh.tm.entity;

import ru.dragosh.tm.enumeration.Status;

public interface Entity {
    String getId();
    String getName();
    void setName(String name);
    String getDateStart();
    void setDateStart(String dateStart);
    String getDateFinish();
    void setDateFinish(String dateFinish);
    String getUserId();
    void setUserId(String userId);
    Long getSystemTime();
    Status getStatus();
    void setStatus(Status status);
}
