package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public abstract class AbstractService <E extends Entity, R extends Repository<E>>{
    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    public void persist(@NotNull final E entity) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateFinish())));
        repository.persist(entity);
    }

    public void merge(@NotNull final E entity) {
        repository.merge(entity);
    }

    public void remove(@NotNull final String userId, @NotNull final String entityId) {
        repository.remove(userId, entityId);
    }
}
