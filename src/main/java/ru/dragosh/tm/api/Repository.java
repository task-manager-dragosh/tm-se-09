package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Entity;

import java.text.ParseException;
import java.util.List;

public interface Repository<E extends Entity> {
    void persist(E entity) throws ParseException;
    void merge(E entity);
    void remove(String userId, String entityId);


    List<E> getSortedBySystemTime(String userId);
    List<E> getSortedByDateStart(String userId);
    List<E> getSortedByDateFinish(String userId);
    List<E> getSortedByStatus(String userId);
}
