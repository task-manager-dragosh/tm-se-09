package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.text.ParseException;
import java.util.List;

public interface ProjectService {
    List<Project> findAll(String userId);
    Project find(String projectName, String userId);
    void persist(Project project) throws ParseException;
    void merge(Project project);
    void remove(String userId, String projectId);
    void removeAll(String userId);

    List<Project> findByStringPart(String userId, String str);

    List<Project> getSortedBySystemTime(String userId);
    List<Project> getSortedByDateStart(String userId);
    List<Project> getSortedByDateFinish(String userId);
    List<Project> getSortedByStatus(String userId);
}