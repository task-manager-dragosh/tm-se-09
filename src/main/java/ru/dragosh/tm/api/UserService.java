package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.User;

public interface UserService {
    User find(String login, String password);
    User findByLogin(String login);
    void persist(User user);
    void merge(User user);
    void remove(String userId);
}
